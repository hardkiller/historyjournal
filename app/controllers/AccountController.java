package controllers;

import play.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;
import play.mvc.*;
import play.data.validation.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.mail.*;

import play.libs.Mail;



import models.*;
import utils.*;

public class AccountController extends Controller {

    public static final String DATE_STRING_FORMAT = "dd.MM.yyyy";


    public static void update() {

        Account jsonAccount = new GsonBuilder().create().fromJson(request.params.get("body"), Account.class);

        Account account = (jsonAccount.id > 0) ? (Account) Account.findById(jsonAccount.id) :  new Account ();

        account.login = jsonAccount.login;
        account.password = jsonAccount.password;
        account.email = jsonAccount.email;
        account.comment = jsonAccount.comment;
        account.lastName = jsonAccount.lastName;
        account.firstName = jsonAccount.firstName;
        account.middleName = jsonAccount.middleName;
        account.photo = jsonAccount.photo;
        account.comment = jsonAccount.comment;
        account.role = jsonAccount.role;

        account.save();

        renderJSON(account);
    }

    public static void restoreMail () {

//        SimpleEmail email = new SimpleEmail();
//        email.setHostName("smtp.googlemail.com");
//        email.setSmtpPort(465);
//        email.setAuthenticator(new DefaultAuthenticator("kurenkov63", "GalaxyGuide"));
//        email.setSSLOnConnect(true);
//        email.setFrom("kurenkov63@gmail.com");
//        email.setSubject("TestMail");
//        email.setMsg("This is a test mail  RESTORE YOUR PASSWORD, YOPTA... :-)");
//        email.addTo("kurenkovAA1229@rambler.ru");
//        email.send();


//        SimpleEmail email = new SimpleEmail();
//        email.setFrom("kurenkovAA1229@rambler.ru");
//        email.addTo("kurenkov63@gmail.com");
//        email.setSubject("Restoring password at historyjournal.ru");
//        email.setMsg("Restoring fuckin password");
//        Mail.send(email);

    }

    public static void delete(Long id) {
        Account account = Account.findById(id);
        account.delete ();
        renderText("success");
    }

    public static void show(Long id)  {

        Account account = (id == -1) ? Security.getProfile () : (Account) Account.findById(id);

        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(account);
        renderJSON(json);
    }

    public static void list() {
        List<Account> accounts = Account.find("FROM Account a").fetch();

        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(accounts);
        renderJSON(json);
    }

    public static void profile () {
        Account account = Security.getProfile ();
        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(account);
        renderJSON(json);
    }
}