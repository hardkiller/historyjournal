package controllers;

import play.mvc.*;
import play.data.validation.*;

import java.util.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import models.*;

public class ItemController extends Controller {

//    public static final String DATE_STRING_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_STRING_FORMAT = "dd.MM.yyyy";

    public static void update() {

        Item jsonItem = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create().fromJson(request.params.get("body"), Item.class);

        Item item = (jsonItem.id > 0) ? (Item) Item.findById(jsonItem.id) :  new Item ();

        item.firstName = jsonItem.firstName;
        item.picture = jsonItem.picture;
        item.regionId = jsonItem.regionId;
        item.lastName = jsonItem.lastName;
        item.middleName = jsonItem.middleName;
        item.birthDate = jsonItem.birthDate;
        item.title = jsonItem.title;
        item.pinned = jsonItem.pinned;
        item.pictureId = jsonItem.pictureId;
        item.message = jsonItem.message;
        item.itemType = jsonItem.itemType;


        item.date = jsonItem.date;
        item.userId = null;
        item.acceptedByUserId = null;


        item.save();
        renderJSON(item);
    }

    public static void delete(Long id) {
        Item item = Item.findById(id);
        item.delete ();
        renderText("success");
    }

    public static void show(Long id)  {
        Item item = Item.findById(id);

        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(item);
        renderJSON(json);
    }


    public static void linkObjects() {

        String fromObj = params.get ("fromobj");
        String toObj = params.get ("toobj");

        ItemLink link = new ItemLink ();

        link.fromItem = Long.parseLong (fromObj);
        link.toItem = Long.parseLong (toObj);
        link.twoWay = true;
        link.save ();

        Gson gson = new GsonBuilder().setDateFormat( DATE_STRING_FORMAT).create();
        String json = gson.toJson(link);
        renderJSON(link);
    }



    public static void todayEvents () {

        java.util.Date date= new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Integer month = cal.get(Calendar.MONTH) + 1;
        Integer day = cal.get(Calendar.DAY_OF_MONTH);

        String query = "FROM Item i WHERE i.itemType='dateofday' AND DAYOFMONTH(i.date)=" + day.toString() + " AND MONTH(i.date)= " + month.toString ();

        List<Item> items = Item.find(query).fetch();

        Gson gson = new GsonBuilder().setDateFormat( DATE_STRING_FORMAT).create();
        String json = gson.toJson(items);
        renderJSON(json);
    }


    public static void getLinkedObjects (Long id) {


        String query = "FROM ItemLink i WHERE i.fromItem='" +id.toString() + "' OR i.toItem='" + id.toString() + "' ";


        List<ItemLink> itemLinks = ItemLink.find(query).fetch();

        List<Long> queryItems = new ArrayList<Long>();

        for ( ItemLink itemLink : itemLinks) {

            if (itemLink.fromItem != id) {

                queryItems.add (itemLink.fromItem);
            }

            if (itemLink.toItem != id) {

                queryItems.add (itemLink.toItem);
            }
        }


        String tmp = params.get ("itemType");

        String itemType = null;

        if ("people".equals(tmp)) {
            itemType = tmp;
        } else if ("news".equals (tmp)) {
            itemType = tmp;
        } else if ("place".equals (tmp)) {
            itemType = tmp;
        } else if ("event".equals (tmp)) {
            itemType = tmp;
        } else if ("rule".equals (tmp)) {
            itemType = tmp;
        } else if ("help".equals (tmp)) {
            itemType = tmp;
        } else if ("project".equals (tmp))  {
            itemType = tmp;
        } else if ("village".equals (tmp)) {
            itemType = tmp;
        } else if ("route".equals (tmp)) {
            itemType = tmp;
        } else if ("dateofday".equals (tmp)) {
            itemType = tmp;
        }

        String q2 = "FROM Item i WHERE i.id IN (:items) ";

        if (itemType != null) {

            q2 += " AND i.itemType= '" + itemType + "' ";
        }

        List<Item> items = Item.find(q2).setParameter("items", queryItems).fetch();


        Gson gson = new GsonBuilder().setDateFormat( DATE_STRING_FORMAT).create();
        String json = gson.toJson(items);
        renderJSON(json);
    }

    public static void list() {

        String moderate = params.get ("moderate");

        if ("moderate".equals (moderate)) {

            String query = "FROM Item i";

            List<Item> items = Item.find(query).fetch();
            Gson gson = new GsonBuilder().setDateFormat( DATE_STRING_FORMAT).create();
            String json = gson.toJson(items);
            renderJSON(json);
            return;
        }

        String tmp = params.get ("itemType");

        String reg = params.get ("regionId");

        String itemType = null;

        if ("people".equals(tmp)) {
            itemType = tmp;
        } else if ("news".equals (tmp)) {
            itemType = tmp;
        } else if ("place".equals (tmp)) {
            itemType = tmp;
        } else if ("event".equals (tmp)) {
            itemType = tmp;
        } else if ("rule".equals (tmp)) {
            itemType = tmp;
        } else if ("help".equals (tmp)) {
            itemType = tmp;
        } else if ("project".equals (tmp))  {
            itemType = tmp;
        } else if ("village".equals (tmp)) {
            itemType = tmp;
        } else if ("route".equals (tmp)) {
            itemType = tmp;
        } else if ("dateofday".equals (tmp)) {
            itemType = tmp;
        }

        String query = "FROM Item i WHERE 1=1 ";

        if (! "village".equals (itemType)) {
            query += " AND i.itemType <> 'village' ";
        }

        if (itemType != null) {
            query += " AND i.itemType='" + itemType + "' ";
        } else {

            query += " AND i.itemType IN ('place', 'event', 'people', 'route') ";

            if (reg == null) {
                query += "AND i.itemType <> 'village'"; //" AND i.pinned <> 1";
            }

        }


        if (reg != null) {
//            query += "AND i.regionId=" + reg + " ";
            query += "AND i.regionId=" + reg + " AND i.id <> " + reg + " ";

            query += " OR (i.pinned = 1 AND i.regionId =" + reg + " AND i.itemType='village' AND i.id <> " + reg +")";

//            query += "AND i.regionId=" + reg + " AND i.id <> " + reg + " ";
        } else {

            if (!"village".equals(itemType))  {

                query += " AND i.itemType != 'village' ";
            } else {
//                query += " AND i.pinned <> 1";
            }
//

            if ("place".equals(itemType))  {

                query += " AND i.pinned IS NULL OR i.pinned=0 ";
            }
        }

        query += " ORDER BY i.pinned DESC, i.id DESC ";

        List<Item> items = Item.find(query).fetch();

        Gson gson = new GsonBuilder().setDateFormat( DATE_STRING_FORMAT).create();
        String json = gson.toJson(items);
        renderJSON(json);
    }
}