package controllers;

import play.mvc.*;
import play.data.validation.*;

import java.util.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.*;

public class Security extends Secure.Security {

    static boolean authenticate(String username, String password) {

        String query = "FROM Account WHERE login=:login AND password=:password";

        List<Account> list = Account
                .find(query)
                .bind("login", username)
                .bind("password", password)
                .fetch(1);

        return (list.size() == 1);
    }



    static Account getProfile () {

        String username = connected ();

        if (username == null) {
            return new Account ();
        }

        String query = "FROM Account a WHERE a.login=:login";

        Account account = Account.find(query)
                .bind("login", username)
                .first();

        if (account == null) {
            return new Account();
        }
        return account;
    }

    static boolean check(String profile) {

//        System.out.println ("CHECK grants for profile : " + profile);

        return true;
    }
}