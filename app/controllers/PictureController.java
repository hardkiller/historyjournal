package controllers;

import play.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;
import play.mvc.*;
import play.data.validation.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.*;
import utils.*;

public class PictureController extends Controller {


    public static void addPicture(Picture picture) {

        Item item = (picture.itemId > 0) ? (Item) Item.findById(picture.itemId) :  new Item ();

        picture.save();

        if (item.id > 0 && picture.itemId != null && picture.itemId > 0) {
            item.pictureId = picture.id;
            item.save();
        }

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(picture);
        renderJSON(json);
    }

    public static void index () {
        render();
    }

    public static void getPicture(long id) {
        final Picture picture = Picture.findById(id);
        response.setContentTypeIfNotSet(picture.photo.type());
        java.io.InputStream binaryData = picture.photo.get();
        renderBinary(binaryData);
    }
}