package controllers;

import play.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;
import play.mvc.*;
import play.data.validation.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.*;
import utils.*;

public class RegionController extends Controller {

    public static final String DATE_STRING_FORMAT = "dd.MM.yyyy";


    public static void update() {

        Region jsonRegion = new GsonBuilder().create().fromJson(request.params.get("body"), Region.class);

        Region region = (jsonRegion.id > 0) ? (Region) Region.findById(jsonRegion.id) :  new Region ();

        region.code = jsonRegion.code;
        region.name = jsonRegion.name;
        region.picture = jsonRegion.picture;
        region.comment = jsonRegion.comment;

        region.save();

        renderJSON(region);
    }

    public static void delete(Long id) {
        Region region = Region.findById(id);
        region.delete ();
        renderText("success");
    }


    public static void show(Long id)  {
        Region region = Region.findById(id);

        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(region);
        renderJSON(json);
    }


    public static void list() {
        List<Region> regions = Region.find("FROM Region a").fetch();

        Gson gson = new GsonBuilder().setDateFormat(DATE_STRING_FORMAT).create();
        String json = gson.toJson(regions);
        renderJSON(json);
    }
}