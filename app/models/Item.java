package models;

import org.hibernate.annotations.Type;
import play.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class Item extends Model {

    public String firstName;

    public String lastName;

    public String middleName;

    public String itemType;

    public String picture;

    public Long pictureId;

    public Integer regionId;

    public Integer pinned;

    @Type(type="date")
    public Date birthDate;

    public String title;
    public String message;

    @Type(type="date")
    public Date date;

    public Integer userId;

    public Integer acceptedByUserId;

}
