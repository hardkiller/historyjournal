package models;

import org.hibernate.annotations.Type;
import play.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class ItemLink extends Model {

    public Long fromItem;
    public Long toItem;
    public Boolean twoWay;
}
