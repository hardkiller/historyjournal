package models;

import play.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class Picture extends Model {

    @Required
    public Blob photo;

    @Required
    public Long itemId;

    @Required
    public String comment;
}
