package models;

import org.hibernate.annotations.Type;
import play.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.*;


@Entity
public class Region extends Model {

    public String code;
    public String name;
    public String picture;
    public String comment;
}
