package models;

import play.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class Account extends Model {

    @Required
    public String login;
    @Required
    public String password;
    @Required
    public String email;
    @Required
    public String lastName;
    @Required
    public String firstName;
    @Required
    public String middleName;
    @Required
    public String photo;
    @Required
    public String comment;
    @Required
    public String role;
}
