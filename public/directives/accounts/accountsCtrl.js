app.controller('AccountsCtrl', ["$scope",  "$resource",  function ($scope, $resource) {

      $scope.itemsResource = $resource ('/accounts/:id');;
      $scope.accountsList = $scope.itemsResource.query();

}]);