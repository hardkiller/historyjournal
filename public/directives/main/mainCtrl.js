app.controller('MainCtrl', ["$scope", "$resource","$rootScope", "$location", function ($scope, $resource, $rootScope, $location) {

    $scope.todaydate = new Date();

    $scope.itemsResource = $resource ('/items/:id');;
    $scope.newsList = null;// $scope.itemsResource.query({ "itemType" : "news"});

    $scope.showVillage = false;
    $scope.todayEvent = 0;

    $scope.regionId = null;


    $scope.slickConfig = {
        enabled: true,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };


    $scope.runNews = function () {

        $location.path ("/#/news");
    }
//    $scope.viewVal = null;

//    $scope.$watch ('viewVal', function () {
//
//        $rootScope.viewVal = $scope.viewVal;
//    })




    $scope.$watch ('regionId', function () {

           $rootScope.regionId = $scope.regionId;

           if (!$scope.regionId) {

                $scope.newsList = $scope.itemsResource.query({ "itemType" : "news"});
                return;
           }
            $scope.showVillage = true;
            $scope.newsList = $scope.itemsResource.query({ "itemType" : 'village' , "regionId": $scope.regionId});
    });


//    $scope.todayEvents = null;

    $scope.todayEvents = $resource ('/todayevents').query ({}, function (){
//            $scope.todayEvents = $scope.todayEventsQ;
            console.log("LENGTH OF EVENTS", $scope.todayEvents.length);
    });


    $scope.prevEvent = function () {

        if ($scope.todayEvent > 0) {
            $scope.todayEvent = $scope.todayEvent - 1;
        }
    }

    $scope.nextEvent = function () {
//    alert('next');
        if ($scope.todayEvent < $scope.todayEvents.length - 1) {
            $scope.todayEvent = $scope.todayEvent + 1;
        }
    }

}]);