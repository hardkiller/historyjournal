'use strict';
angular.module('angularDateTimePicker', [])
    .directive('dateTimePicker', [function () {

        return {
            replace: true,
            restrict: 'A',
            scope: {
                value: '=ngModel',
                inline: "="
            },
            template: "<input class='datetimepicker' type='text' />",
            link: function ($scope, $el) {

                var inputValueChanged = function (value) {
                    $scope.value = value;
                };

                var createDatePicker = function (id) {
                    jQuery.datetimepicker.setLocale('ru');
                    $('#' + id).datetimepicker({
                        timepicker:false,
                        inline: $scope.inline,
                        datepicker: true,
                        defaultDate: new Date(),
                        onChangeDateTime:function(dp,$input){
                            inputValueChanged($input.val())
                        },
                        format:'d.m.Y'
                    })
                };

                var guid = function guid() {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                    }
                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +  s4() + '-' + s4() + s4() + s4();
                };

                var id = guid();
                $el.attr('id', id);
                createDatePicker(id);
            }
        };
    }]
);