app.controller('RegionEditCtrl',  ["$scope", "$resource", "$location", "toaster",  function ($scope, $resource, $location, toaster) {


    $scope.itemsResource = $resource ('/regions/:id');

    $scope.code = "";
    $scope.name = "";
    $scope.picture = "";
    $scope.comment = "";

    $scope.saveProcess = false;


    $scope.saveRecord = function () {

            if($scope.saveProcess) {
                return;
            }

            $scope.saveProcess = true;

            var model = {
                    id: 0,
                    code: $scope.code,
                    name: $scope.name,
                    picture: $scope.picture,
                    comment: $scope.comment
            };

            $scope.itemsResource.save (model, function (){

                  toaster.pop( "success", "", "Запись успешно добавлена!");
                  $scope.saveProcess = false;
                  $location.path ("/regions");
            });
    };

    $scope.cancel = function () {
            $location.path ("/regions");
    };


}]);