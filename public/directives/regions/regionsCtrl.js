app.controller('RegionsCtrl', ["$scope",  "$resource",  function ($scope, $resource) {

      $scope.itemsResource = $resource ('/regions/:id');;
      $scope.regionsList = $scope.itemsResource.query();

}]);