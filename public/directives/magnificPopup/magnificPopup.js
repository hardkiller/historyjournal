'use strict';
angular.module('magnificPopup', [])
    .directive('magnificPopup', [function () {

        return {
            replace: true,
            restrict: 'A',
            template: "<div>Gallery</div>",
            link: function ($scope, $el) {

                var createMagnific = function (id) {

//                    var attr = {
//
//                        items: [
//
//                              {
//                                src: '#test-modal', // CSS selector of an element on page that should be used as a popup
//                                type: 'inline'
//                              }
//                        ]
//                    };
//                    $(id).magnificPopup(attr);
//                     $.magnificPopup.open({
//                            items: {
//                                src: $('#test-popup')[0].outerHTML
//                            },
//                            type: 'inline'
//                        });
                };

                var guid = function guid() {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                    }
                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +  s4() + '-' + s4() + s4() + s4();
                };

                var id = guid();
                $el.attr('id', id);
                createMagnific(id);
            }
        };
    }]
);
