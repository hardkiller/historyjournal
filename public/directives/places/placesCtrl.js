app.controller('PlacesCtrl', ["$scope", "$resource",   function ($scope, $resource) {


    $scope.itemsResource = $resource ('/items/:id');;
    $scope.newsList = $scope.itemsResource.query({ "itemType" : "news"});

    $scope.eventsList = $scope.itemsResource.query({ "itemType" : "place"});

    $scope.todayEvent = 0;

    $scope.todayEvents = $resource ('/todayevents').query ({}, function (){
//            console.log("LENGTH OF EVENTS", $scope.todayEvents.length);
    });


    $scope.prevEvent = function () {

        if ($scope.todayEvent > 0) {
            $scope.todayEvent = $scope.todayEvent - 1;
        }
    }

    $scope.nextEvent = function () {
        if ($scope.todayEvent < $scope.todayEvents.length - 1) {
            $scope.todayEvent = $scope.todayEvent + 1;
        }
    }

}]);