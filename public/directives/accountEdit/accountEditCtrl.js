app.controller('accountEditCtrl',  ["$scope", "$resource", "$location", "toaster",  function ($scope, $resource, $location, toaster) {


    $scope.itemsResource = $resource ('/accounts/:id');

    $scope.selectedItemType = null;

    $scope.itemTypes = [
        {alias: "guest",  name: "Посетитель" },
        {alias: "moderator", name: "Модератор" },
        {alias: "admin",  name: "Администратор" },
    ];

    $scope.onSelectItemType = function ($item, $model) {
        $scope.selectedItemType = $model;
    }

    $scope.login = "";
    $scope.password = "";
    $scope.email = "";
    $scope.firstName = "";
    $scope.lastName = "";
    $scope.middleName = "";
    $scope.photo = "";
    $scope.comment = "";

    $scope.saveProcess = false;


    $scope.saveRecord = function () {

            $scope.saveProcess = true;

            var model = {
                    id: 0,
                    role: $scope.selectedItemType.alias || null,
                    login: $scope.login,
                    password: $scope.password,
                    email: $scope.email,
                    firstName: $scope.firstName,
                    lastName: $scope.lastName,
                    middleName: $scope.middleName,
                    photo: $scope.photo,
                    comment: $scope.comment,
            };

            $scope.itemsResource.save (model, function (){

                  toaster.pop( "success", "", "Запись успешно добавлена!");
                  $scope.saveProcess = false;
                  $location.path ("/");
            });
    };

    $scope.cancel = function () {
            $location.path ("/");
    };


}]);