app.controller('RulesCtrl', ["$scope", "$resource",  function ($scope, $resource) {

        $scope.itemsResource = $resource ('/items/:id');;
        $scope.rulesList = $scope.itemsResource.query({ "itemType" : "rule"});
}]);