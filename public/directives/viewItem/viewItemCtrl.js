app.controller('ViewItemCtrl', ["$scope", "$resource", "$stateParams", "$rootScope", function ($scope, $resource, $stateParams, $rootScope) {

        $scope.item = null;
        $scope.linkObjectsList = null;

        var editId =  $stateParams.id;

        $scope.usr = $rootScope.usr;

        $scope.itemsResource = $resource ('/items/:id');

        $scope.itemsResource.get ({id : editId}, function (data) { $scope.item = data; })

        $scope.getLinkedObjects = function (id) {

              $scope.linkObjectsResource = $resource ('/linkedobjects/:id');;
              $scope.linkObjectsList = $scope.linkObjectsResource.query({id: id }, function () {
              });
        }

        $scope.getLinkedObjects (editId);
}]);