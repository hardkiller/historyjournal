app.controller('Modal1Ctrl', ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close({value : 0});
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);