app.controller('AddLinkModalCtrl', ["$scope", "$resource", "close", "itemType", function ($scope, $resource, close, itemType) {

         $scope.placesResource = $resource ('/items/:id');
         $scope.placesList = $scope.placesResource.query({ "itemType" : itemType});

         $scope.close = close;

         $scope.selectObject = function (pl) {

                $scope.close (pl);
         }
}]);