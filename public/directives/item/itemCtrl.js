app.controller('ItemCtrl', ["$scope", "$rootScope", "$resource", "$location", "toaster",  "$stateParams", 'Upload', "$timeout",  "ModalService",
            function ($scope, $rootScope, $resource, $location, toaster, $stateParams, Upload, $timeout, ModalService) {


    if ($rootScope.usr == null) {

        $location.path ("/#/");
    };


    $scope.model = {
        id: 0,
        date: null,
        itemType: "event",
        title: "",
        firstName: "",
        lastName: "",
        picture: "",
        pinned: false,
        pictureId: null,
        regionId: null,
        middleName: "",
        birhDate: null,
        message: ""
    };


    $scope.options = {
        language: 'ru',
        allowedContent: true,
        entities: false
      };

    $scope.regionsList = null;

    $scope.linkObjectsListPeople  = null;
    $scope.linkObjectsListPlace  = null;

    $scope.getLinkedObjects = function (id) {

            $scope.linkObjectsResource = $resource ('/linkedobjects/:id');;
            $scope.linkObjectsPeopleList = $scope.linkObjectsResource.query({id: id, itemType: "people" }, function () {

            });
            $scope.linkObjectsPlaceList = $scope.linkObjectsResource.query({id: id, itemType: "place" }, function () {


            });
    }

    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: '/picture',
                data: { "picture.photo": file, "picture.itemId": $scope.model.id, "picture.comment" : "2128506"}
            });

            file.upload.then(function (response) {
                $timeout(function () {
//                    file.result = response.data;
                    $scope.model.pictureId = response.data.id;
                });
            }, function (response) {
//                if (response.status > 0)
//                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
//                file.progress = Math.min(100, parseInt(100.0 *
//                                         evt.loaded / evt.total));
            });
        }
    }


    $scope.addNewLink = function (itemType) {

            ModalService.showModal({
                templateUrl: "public/directives/addLinkModal/addLinkModal.html",
                controller: "AddLinkModalCtrl",
                inputs: {
                    itemType: itemType,
                  }

//                resolve: {
//                         itemType: function () {
//                           return itemType;
//                         }
//                       }
              }).then(function(modal) {

                modal.itemType = itemType;
                //it's a bootstrap element, use 'modal' to show it
                modal.element.modal();
                modal.close.then(function(result) {

                    if (result != null) {

                           $scope.regionsResource = $resource ('/link/:id');;
                            $scope.regionsList = $scope.regionsResource.get({fromobj: result.id, toobj: $scope.model.id}, function () {

                                    $scope.getLinkedObjects ($scope.model.id);
                            });
                     }
                });
              });

    }


    $scope.checkRegion = function () {

            if ($scope.model.regionId != null && $scope.regionsList && $scope.regionsList.length > 0) {

                    for (var i = 0; i < $scope.regionsList.length; i++) {

                        if ($scope.regionsList[i].id == $scope.model.regionId) {
                         $scope.selectedRegion = $scope.regionsList[i];
                        }
                    }
            }
    }

    $scope.regionsResource = $resource ('/regions/:id');;
    $scope.regionsList = $scope.regionsResource.query({}, function () {

        $scope.checkRegion ();
    });

    $scope.selectedRegion = null;


    $scope.onSelectRegion = function ($item, $model) {
        $scope.selectedRegion = $model;
        $scope.model.regionId = $model.id;
    }



    $scope.itemTypes = [
        {alias: "news",   name: "Новость" },
        {alias: "people", name: "Человек" },
        {alias: "place",  name: "Место" },
        {alias: "event",  name: "Событие" },
        {alias: "rule",   name: "Правило" },
        {alias: "help",   name: "Справка" },
        {alias: "project", name: "О проекте" },
    ];

    $scope.selectedItemType = null;

    $scope.isPeople = false;


    $scope.onSelectItemType = function ($item, $model) {
        $scope.selectedItemType = $model;

        $scope.model.itemType = $scope.selectedItemType.alias;

        if ($model.alias == "people") {
            $scope.isPeople = true;
        } else {
            $scope.isPeople = false;
        }
    }


    $scope.itemsResource = $resource ('/items/:id');

    if ($stateParams.id != null) {

        var mdl = $scope.itemsResource.get({id: $stateParams.id}, function () {
                if (mdl.id) {

                    $scope.model = {
                            id:  mdl.id || 0,
                            date: mdl.date || null,
                            itemType: mdl.itemType || "event",
                            title: mdl.title || "",
                            firstName: mdl.firstName || "",
                            lastName: mdl.lastName ||  "",
                            picture: mdl.picture || "",
                            pictureId: mdl.pictureId || null,
                            regionId: mdl.regionId || null,
                            middleName: mdl.middleName || "",
                            birthDate: mdl.birthDate ||  null,
                            pinned: (mdl.pinned == 1 ? true : false),
                            message: mdl.message || ""
                    };

                    $scope.checkRegion ();

                    $scope.getLinkedObjects (mdl.id);

                     for (var i = 0; i < $scope.itemTypes.length; i++) {

                           if ($scope.itemTypes[i].alias == $scope.model.itemType) {
//                                  $scope.selectedItemType = $scope.itemTypes[i];
                                   $scope.onSelectItemType(null, $scope.itemTypes[i])

                           }
                     }
                }
         });
    }



    $scope.testPopup = function () {

     toaster.pop( "success", "", "Запись успешно сохранена!");
    }



    $scope.saveProcess = false;

    $scope.saveRecord = function () {

            $scope.saveProcess = true;


            $scope.model.pinned = $scope.model.pinned ? 1: 0;

            $scope.itemsResource.save ($scope.model, function (){

                    toaster.pop( "success", "", "Запись успешно сохранена!");
                    $scope.saveProcess = false;
                    $location.path ("/");
            });
    };


    $scope.cancel = function () {
            $location.path ("/");
    };


}]);