var app = angular.module("historicPortal", [
         'ngAnimate',
         'ngTable',
         'toaster',
         'ui.bootstrap',
         'ui.router',
         'ui.select',
         'restangular',
         'summernote',
         'angularDateTimePicker',
         'ngResource',
         'ngSanitize',
         'angularHighmap',
         'ckeditor',
         'slick',
         'ngFileUpload',
         'magnificPopup',
         'angularModalService'
     ]);


app.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/main");
  //
  // Now set up the states
  $stateProvider
    .state('main', {
        url: "/main",
        templateUrl: "public/directives/main/main.html"
      })
     .state('moderate', {
        url: "/moderate",
        templateUrl: "public/directives/moderate/moderate.html"
      })
      .state('accounts', {
        url: "/accounts",
        templateUrl: "public/directives/accounts/accounts.html"
      })
      .state('feedback', {
        url: "/feedback",
        templateUrl: "public/directives/feedback/feedback.html"
      })

      .state('regions', {
        url: "/regions",
        templateUrl: "public/directives/regions/regions.html"
      })
     .state('view_item', {
              url: "/view_item/:id",
              templateUrl: "public/directives/viewItem/viewItem.html"
     })
     .state('regionedit', {
        url: "/regionedit",
        templateUrl: "public/directives/regionEdit/regionEdit.html"
      })
    .state('accountedit', {
        url: "/accountedit",
        templateUrl: "public/directives/accountEdit/accountEdit.html"
      })
    .state('profile', {
      url: "/profile",
      templateUrl: "public/directives/profile/profile.html"
    })
    .state('events', {
      url: "/events",
      templateUrl: "public/directives/events/events.html"
    })
    .state('help', {
      url: "/help",
      templateUrl: "public/directives/help/help.html"
    })
    .state('news', {
      url: "/news",
      templateUrl: "public/directives/news/news.html"
    })
    .state('people', {
      url: "/people",
      templateUrl: "public/directives/people/people.html"
    })
    .state('places', {
      url: "/places",
      templateUrl: "public/directives/places/places.html"
    })
    .state('project', {
      url: "/project",
      templateUrl: "public/directives/project/project.html"
    })
    .state('rules', {
      url: "/rules",
      templateUrl: "public/directives/rules/rules.html"
    })
    .state('item', {
      url: "/item/:id",
      templateUrl: "public/directives/item/item.html"
    });
});


app.controller("appCtrl", ["$resource", "$scope", "$rootScope", "$timeout", "$location", function($resource, $scope, $rootScope, $timeout, $location) {
    $scope.message = "";
    $scope.messages = [];

    $scope.usr = null;
    $rootScope.usr = null;

    $scope.viewVal = null;


    $scope.profileResource = $resource ('/profile');;

     var usr = $scope.profileResource.get({}, function () {

         if (usr.id) {
              $scope.usr = usr;
              $rootScope.usr = usr;
         } else {
              $scope.usr = null;
              $rootScope.usr = null;
         }
     });

    $scope.regionsResource = $resource ('/regions/:id');;
    $scope.regionsList = $scope.regionsResource.query({});


    $scope.eventsResource = $resource ('/items/:id');;
    $scope.eventsList = $scope.eventsResource.query({ "itemType" : "event", "regionId": $rootScope.regionId});


    $scope.people = function () {

        if ($scope.viewVal == 'people') {

               $scope.viewVal = null;
        } else {
                $scope.viewVal = 'people';
        }

        $scope.eventsResource = $resource ('/items/:id');;
        $scope.eventsList = $scope.eventsResource.query({ "itemType" : $scope.viewVal, "regionId": $rootScope.regionId});
    }

    $scope.places = function () {

        if ($scope.viewVal == 'place') {

               $scope.viewVal = null;
        } else {
                $scope.viewVal = 'place';
        }

            $scope.eventsResource = $resource ('/items/:id');;
            $scope.eventsList = $scope.eventsResource.query({ "itemType" : $scope.viewVal , "regionId": $rootScope.regionId})
    }

    $scope.events = function () {

            if ($scope.viewVal == 'event') {

                   $scope.viewVal = null;
            } else {
                    $scope.viewVal = 'event';
            }

            $scope.viewVal = 'event';
            $scope.eventsResource = $resource ('/items/:id');;
            $scope.eventsList = $scope.eventsResource.query({ "itemType" : $scope.viewVal , "regionId": $rootScope.regionId})
    }

     $scope.getItems = function (val) {

               if (val == $scope.viewVal) {

                   $scope.viewVal = null;
               } else {

                   $scope.viewVal = val;
               }
               $scope.eventsResource = $resource ('/items/:id');
               $scope.eventsList = $scope.eventsResource.query({ "itemType" : $scope.viewVal , "regionId": $rootScope.regionId});
       }


    $scope.selectedRegion = null;


    $scope.runAtMain = function () {

        $scope.selectedRegion = null;

        $location.path ("/#/main");
    }

    if ($scope.regionsList != null && $scope.regionsList.length > 0) {

        $scope.selectedRegion = $scope.regionsList[0];
    };

    $scope.onSelectRegion = function ($item, $model) {

//        $scope.selectedRegion = $model;

        for (var i = 0; i < $scope.regionsList.length; i++) {

            if ($scope.regionsList[i].id == $model.id) {

                    $scope.selectedRegion = $scope.regionsList[i];
                    $rootScope.regionId = $scope.selectedRegion.id;
                    $scope.getItems ($scope.viewVal);

            }
        }


    }

    $rootScope.$watch ('regionId', function () {

        if ($rootScope.regionId != null) {
            $scope.onSelectRegion (null, {id: $rootScope.regionId});
        }
    });

}]);
